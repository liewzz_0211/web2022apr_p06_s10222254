using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Authentication.Cookies;
using Google.Apis.Auth.AspNetCore3;

//This describes the routing that takes place and others





namespace web_S10222254
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        //This method gets called by the runtime. Use this method to add services to the container.
        //makes MVC service available in the app
        public void ConfigureServices(IServiceCollection services)
        {





            // Add a default in-memory implementation of distributed cache
            services.AddDistributedMemoryCache();
            // Add the session service
            services.AddSession(options =>
            {
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });





            services.AddControllersWithViews();

            // This configures Google.Apis.Auth.AspNetCore3 for use in this app.
            services.AddAuthentication(options =>
            {
                // Login (Challenge) to be handled by Google OpenID Handler,
                options.DefaultChallengeScheme =
                GoogleOpenIdConnectDefaults.AuthenticationScheme;

                // Once a user is authenticated, the OAuth2 token info
                // is stored in cookies.
                options.DefaultScheme =
                CookieAuthenticationDefaults.AuthenticationScheme;
            })
           .AddCookie()
           .AddGoogleOpenIdConnect(options =>
           {
    // Credentials (stored in appsettings.json) to identify
    // the web app when performing Google authentication
    options.ClientId =
    Configuration["Authentication:Google:ClientId"];
               options.ClientSecret =
    Configuration["Authentication:Google:ClientSecret"];
           });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();  //called to catch any exceptions that occur in subsequent calls 
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection();

            app.UseStaticFiles();  //called to handle requests to the publicly available files e.g. html folder from "wwwroot"
            //if request is not handled by "UseStaticFiles", it is passed on to "UseEndpoints" Middleware which selects a specific MVC Controller and Action

            app.UseRouting();

            app.UseAuthorization();

            app.UseAuthorization();





            app.UseSession();  //adding the session middleware. Order is important here bc i enable the session before i try to access it





            app.UseEndpoints(endpoints =>
            {
                //the default routing for URL
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");  //default value for Controller is "Home", default value for Action is "Index"
            });
        }
    }
}
