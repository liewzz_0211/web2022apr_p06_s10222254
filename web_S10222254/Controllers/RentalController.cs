﻿//in conlusion of week 2, ViewData and @model can be used to pass a data and call them from the Controller to a View using @ViewData["NameOfKey"] and @Model.NameOfKey
//to declare data into ViewData, use ViewData["NameOfKey"] = DataToBePassed
//to declare data into @model, take note of class of returned object in Controller's Action, in View page '@model web_s1234567.Models.NameOfObjectClass'
//to call a passed data using ViewData, must set ViewData["NameOfKey"] = NameOfDataToBePassed
//to call a passed data using @model, must have a returned object from an Action so that "@model web_s1234567.Models.NameOfClassOfReturnedObject" can be used in View to call the public properties of the object returned


//ViewData used to pass small data from Controller to View so that the small data can be viewed in View using @ViewData["NumBooks"]
//In conclusion of this Controller, 4 lists are declared to contain the list of choices for the Rental Calculation
//RentalController() first created to hard code and populate numBooks, discountList lists, where numBooks contains SelectListItem object of range 1-10 and discountList contains info on "Student Membership" & "Birthday Discount"
//First overload method, Calculate() is created. Main puprose: initialize default values for rental object, when validation error occurs, ViewData["ShowResult"] set to false since nothing to show due to error, set default values of rental object will be shown in View
//Second overload method, Calculate(Rental rental). Main purpose: when user enters correct value for Rental class object called rental, rental goes into this method, ViewData["ShowResult"] set to true since will hve rental's calculations to show, calculate properties like RentalFee, RentalRate etc., show View of rental end product





using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;  //exposes the SelectListItem class which is used for binding data to a drop - down list.
                                           //represents the selected item in an instance of the "SelectListItem" class for this case

using web_S10222254.Models;  //exposes the model classes defined in the “Models” folder of our project
namespace web_S10222254.Controllers
{
    public class RentalController : Controller
    {
        //declaring 4 list of choices required for the Rental Calculation

        // List that stores the options for days (duration) of loan
        private List<int> numLoanDays = new List<int> { 2, 5, 10, 20 };

        // List that stores the corresponding rental rate for each options
        private List<double> rentalRates = new List<double> { 1.0, 1.50, 2.50, 5.00 };

        // A list for populating drop-down list of range 1 to 10 (SelectListItem only used for drop down)
        //"SelectListItem" class comes from line 4 using
        private List<SelectListItem> numBooks = new List<SelectListItem>();

        // A list for populating checkbox list, check between "Student Membership" & "Birthday Discount"
        //is a list of RentalDiscount instances, instance can have access to properties Description, DiscountPercent, Selected
        private List<RentalDiscount> discountList = new List<RentalDiscount>();





        // Constructor for the RentalController, used to populate numBooks and discountList lists
        //populate numBooks list with SelectListItem object with string(1-10) for its Value and Text
        //populate discountList list with 2 RentalDiscount objects for "Student Membership (20%)" and "Birthday Discount (10%)"
        public RentalController()
        {
            //Populate the selection list for drop-down list
            //Populate 10 items in "SelectListItem" object for binding to the drop-down list on the rental calculator view
            for (int i = 1; i <= 10; i++)
            {
                numBooks.Add(
                new SelectListItem  //properties of "SelectListItem" include Disabled, Group, Selected, Text, Value
                                    //a new "SelectListItem" object that has a Value of string(i) and Text string(i), where i is int 1 to 10
                {
                    Value = i.ToString(),
                    Text = i.ToString(),
                });
            }
        

            //Populate the selection list for checkboxes list with objects from "RentalDiscount" class
            //add 2 "RentalDiscount" objects which are to be bound to a checkbox list on the rental calculator view
            //there are 2 types of discounts, "Student Membership" & "Birthday Discount" with 20% and 10% discount respectively
            //"discountList" will have 2 RentalDiscount class objects for "Student Membership" & "Birthday Discount" with their respective Description, DiscountPercent & Selected property
            discountList.Add(
            new RentalDiscount
            {
                Description = "Student Membership (20%)",  //"Description" property contains text display for the checkbox
                DiscountPercent = 20,  //"DiscountPercent" property contains discount value for each checkbox choice
                Selected = false  //Bool value which will decide if it was checked or not
            });

            discountList.Add(
            new RentalDiscount
            {
                Description = "Birthday Discount (10%)",
                DiscountPercent = 10,
                Selected = false
            });
        }





        //First overload method, if there is validation error and need to return to the form for displaying error and correction.
        //Will display the form with rental object's initial/default values
        //called when user follows URL "~/Rental/Calculate" since "Calculate" method found in "RentalController"
        //in this method, i am setting default/initial values of rental object to return with View
        public ActionResult Calculate()
         {
            // Stop accessing the action if not logged in
            // or account not in the "Staff" role
            // if user does not enter corerct LoginID and password, Role will not be "Staff". Since Role is not "Staff" will redirect to Index Action of Home Controller instead
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Staff"))
            {   
                return RedirectToAction("Index", "Home");
            }


            //Prepare the ViewData to be used in Calculate.cshtml view
            //"ViewData" is a dictionary of objects that are stored/retrieved using strings as keys
            //ViewData only transfers data from a Controller Action to the corresponding View, not vice-versa. ViewData values will be cleared if redirection occurs
            //transfers the lists (numBooks, numLoanDays) data to ViewData dictionary at index ("NumBooks", "NumDays") respectively. 
            //** e.g., @ViewData["NumBooks"] can be used to pass data of numBooks from RentalController to View 

            ViewData["ShowResult"] = false;  //"ShowResult" set to false, bc there is no result to show yet 
            ViewData["NumBooks"] = numBooks;  //transfering numBooks list into ViewData dict index "NumBooks"
            ViewData["NumDays"] = numLoanDays;  //transfering numLoanDays list into ViewData dict index "NumDays"


            //initializes default values for the rental model object which will be sent to the "Calculate" View
            //these default values are shown when validation error happens
            Rental rental = new Rental
            {
                //LoanDate var default value will be today's date
                LoanDate = DateTime.Today,

                //default value of NumBooks: int(Value = 1) of first SelectListItem class object from numBooks list
                //numBooks drop down list contains 10 objects of SelectListItem class, where Value & Text of each SelectListItem object are both string(1-10)
                //numBooks[0].Value will be Value = 1 of first SelectListItem object from numBooks list
                NumBooks = Convert.ToInt32(numBooks[0].Value),  //before user selects anything, will show "2" in drop down list 

                // Set the default number of days loaned to 2, where numLoanDays[0] = 2
                NumDays = numLoanDays[0],  //will check value "2" before user selects anything  

                //discountList is a list containing 2 RentalDiscount class objects with properties for "Student Membership" & "Birthday Discount"
                Discounts = discountList
            };
            
            //by returning rental, in Calculate.cshtml, @model MUST return the class of returned object here in the Controller
            //class of rental is Rental, so @mode IS declared for Rental
            return View(rental);  

            //rental object will have default values of:
            //  LoanDate: today date
            //, NumBooks: int(Value = 1) of first object in numBooks list
            //, NumDays: 2
            //, Discounts: discountList list containing 2 RentalDiscount class objects for "Student Membership" & "Birthday Discount"
        }





        [HttpPost]  //called when user  clicks at the Compute button, which will send data in a HTML form to the server via the HttpPost method
        //Second overload method will read the data selected by user from the rental object in Action's parameter,
        //compute the rental fee payable and update the computation result in the rental object to
        //route user back to the Calculate View with rental object to display the result of user's rental object
        public ActionResult Calculate(Rental rental)  //rental object keeps data entered by user
        {
            //initiated ViewData here again to ensure drop-down list (numBooks) & radio buttons list (numLoanDays) still contain their values
            //ViewData["NumBooks"] and ViewData["NumDays"] will still contain data from numBooks and numLoanDays list respectively
            ViewData["ShowResult"] = true;  //set to true so that second fieldset on Calculate View will show the result
            ViewData["NumBooks"] = numBooks;
            ViewData["NumDays"] = numLoanDays;

            // rental object contains user input in the Calculate.cshtml view
            // set rental object' DueDate property to LoanDate + rental object's NumDays
            rental.DueDate = rental.LoanDate.AddDays(rental.NumDays);

            // Get rental rate based on number of day loan selection
            //IndexOf returns index of rental.NumDays from numLoanDays list
            int selectedIndex = numLoanDays.IndexOf(rental.NumDays);  //e.g., if rental.NumDays = 5, selectedIndex = 1
            rental.RentalRate = rentalRates[selectedIndex];  //continue e.g., if selectedIndex = 1, rental.RentalRate = 1.50

            // Calculate rental fee
            rental.RentalFee = rental.NumBooks * rental.NumDays * rental.RentalRate;

            // Calculate the discount percentage based on rental fee
            rental.DiscountPercent = 0.0;
            foreach (RentalDiscount discountItem in rental.Discounts)
            {
                if (discountItem.Selected)
                    rental.DiscountPercent += discountItem.DiscountPercent;
            }

            // Calculate the amount payable
            rental.AmountPayable = (int)(rental.RentalFee * (100 - rental.DiscountPercent) / 100);

            // Route to Calculate.cshtml view to display result contained in the rental object
            return View(rental);
        }
    }
}
