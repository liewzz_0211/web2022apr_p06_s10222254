﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using web_S10222254.Models;
namespace web_S10222254.Controllers
{
    public class FineController : Controller
    {
        //called when user follows url: "/fine/calculate" through HttpGet
        //default is HttpGet, but i can also declare it
        public ActionResult Calculate()
        {
            //basicaly user must now log in properly with password and LoginID to enter Fine Controlle's View, else redirect to Index home page
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Staff"))
            {
                return RedirectToAction("Index", "Home");
            }

            //initializing the data for model object 
            //Prepare the ViewData to be use in Calculate.cshtml view
            ViewData["ShowResult"] = false;  //setting "ShowResult" of ViewData to false so that won't print anything in View since it false
                                             //only print the value if ViewData["ShowResult"] is true
            Fine fine = new Fine();  //new model instance called "fine"
            fine.DueDate = DateTime.Today;
            fine.FineRate = 0.50;  //FineRate be 0.5 per day
            return View(fine);  //send model object to Calculate View to display the values 
        }



        //default is not HttpPost therefore need to indicate the annotation if the action is to response to HttpPost only
        //if i want something to happen when a submit button is clicked, HttpPost is required
        //HttpPost happens when you click a submit button in a form with method attribute of “Post”
        //called when user clicks at compute button in View form and post the data entered in from Controller through HttpPost
        [HttpPost]
        public ActionResult Calculate(Fine fine)
        {
            //add this if any of the property of the stated Model class requires validation
            // The fine object contains user inputs from view
            if (!ModelState.IsValid) // validation fails
            {
                return View(fine); // returns the view with errors
            }

            // Calculate the cumulative fine and its breakdown
            double fineTotal = 0.0;
            string fineBreakdown = "";

            for (int count = 1; count <= fine.NumBooksOverdue; count++)
            {
                double fineForEachBook = count * fine.FineRate *
                fine.NumDaysOverdue;
                fineTotal += fineForEachBook;
                fineBreakdown += "Overdue cost for Book " + count + " = $" +
                fineForEachBook.ToString("#,##0.00") +
               "<br />";
            }

            fine.FineAmt = fineTotal;
            // Prepare the ViewData to be used in Calculate.cshtml view
            ViewData["ShowResult"] = true;  //after calculating everything, set "ShowResult" key to true for display of calculated value
            ViewData["FineBreakdown"] = fineBreakdown;  //stored fineBreakdown into "FineBreakdown" key of ViewData
            // Route to Calculate.cshtml view to display result
            return View(fine);
        }
    }
}
