﻿//This is a Controller, each Controller ends with "Controller" and lives in the Controllers folder (e.g. HomeController)




using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using web_S10222254.Models;

using Microsoft.AspNetCore.Authentication.Cookies;
using Google.Apis.Auth.OAuth2;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Google.Apis.Auth;
using static Google.Apis.Auth.GoogleJsonWebSignature;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;


namespace web_S10222254.Controllers
{
    public class HomeController : Controller
    {

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [HttpPost]  //Action "StaffLogin" will ba activated when the form for staff is posted HttpPost by clicking the submit button (will run when enter /Home/etc)
        //The code reads from a "FormCollection" object ("formData" for this scenario) for Login id & password from user
        public ActionResult StaffLogin(IFormCollection formData)
        //there are 2 types: "FormCollection" and "IFormCollection"
        //FormCollection is a class that represents the <Form> tag in HTML, is a class that enables us to access data entries posted through a <Form> in a HTML file
        //IFormCollection is the interface of the FormCollection class, basically a FormCollection class is be created from an IFormCollection interface.
        //for this case, i created an object out of a class inherited from an interface
        {

            string loginID = formData["txtLoginID"].ToString().ToLower();
            string password = formData["txtPassword"].ToString();

            //if user enters correctly for loginID and password, store data for "LoginID", "Role", "NowDate" (when user logged in) into Session
            if (loginID == "abc@npbook.com" && password == "pass1234")  
            {



                // Store Login ID in session with the key “LoginID”
                HttpContext.Session.SetString("LoginID", loginID);

                // Store user role “Staff” as a string in session with the key “Role”
                HttpContext.Session.SetString("Role", "Staff");

                // Store today date and time as date in session with the key "NowDate"
                DateTime NowDateTime = DateTime.Now;  //store when user logged 
                string DateTimeNow = NowDateTime.ToString("dd-MMMM-yy h:mm:ss tt");
                HttpContext.Session.SetString("NowDate", DateTimeNow);



                return RedirectToAction("StaffMain");
            }
            else  //if it don't match, will redirect user to "Index" Action to return "Index" View with TempData message saying wrong Credentials
            {





                // Store an error message in TempData for display at the index view
                TempData["Message"] = "Invalid Login Credentials!";





                return RedirectToAction("Index");
            }
        }

        //returning the View of "Index.cshtml" found in /Views/Home
        public IActionResult Index()
        {
            return View();
        }

        public ActionResult StaffMain()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public async Task<ActionResult> LogOut()
        {
            // Clear authentication cookie
            await HttpContext.SignOutAsync(
            CookieAuthenticationDefaults.AuthenticationScheme);

            //when user clicks on Log out then set log out time
            DateTime LogoutTime = DateTime.Now;

            //get logged in timing set in previous session when user clicked on "Log In"
            string LogInTime = HttpContext.Session.GetString("NowDate");

            //compare logged in and out timings to get time span
            TimeSpan TimeDiff = LogoutTime - DateTime.Parse(LogInTime);

            // Clear all key-values pairs stored in session state
            HttpContext.Session.Clear();
            
            //store TempData for time difference message. Will be shown after redirecting to Index Action
            TempData["TimeDiff"] = "You have logged in for " + TimeDiff.TotalSeconds;
            
            // Call the Index action of Home controller
            return RedirectToAction("Index");  //removes all the objects stored in the Session but will not destroy session cookie stored in client computer 
                                                //session cookie will only be deleted after 20 mins of inactivity
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [Authorize]
        public async Task<ActionResult> StudentLogin()
        {
            // The user is already authenticated, so this call won't
            // trigger login, but it allows us to access token related values.
            AuthenticateResult auth = await HttpContext.AuthenticateAsync();
            string idToken = auth.Properties.GetTokenValue(
             OpenIdConnectParameterNames.IdToken);
            try
            {
                // Verify the current user logging in with Google server
                // if the ID is invalid, an exception is thrown
                Payload currentUser = await
                GoogleJsonWebSignature.ValidateAsync(idToken);
                string userName = currentUser.Name;
                string eMail = currentUser.Email;
                HttpContext.Session.SetString("LoginID", userName + " / "
                + eMail);
                HttpContext.Session.SetString("Role", "Student");
                HttpContext.Session.SetString("LoggedInTime",
                DateTime.Now.ToString());
                return RedirectToAction("Index", "Book");
            }
            catch (Exception e)
            {
                // Token ID is may be tempered with, force user to logout
                return RedirectToAction("LogOut");
            }
        }



    }
}
