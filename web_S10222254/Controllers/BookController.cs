﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using web_S10222254.Models;
using Microsoft.AspNetCore.Authorization;

namespace web_S10222254.Controllers
{
    [Authorize]
    public class BookController : Controller
    {
        public async Task<ActionResult> Index()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://ictwebapi.azurewebsites.net");
            HttpResponseMessage response = await client.GetAsync("/api/books");
            if (response.IsSuccessStatusCode)
            {
                string data = await response.Content.ReadAsStringAsync();
                List<Book> bookList = JsonConvert.DeserializeObject<List<Book>>(data);
                return View(bookList);
            }
            else
            {
                return View(new List<Book>());
            }
        }
    }
}
