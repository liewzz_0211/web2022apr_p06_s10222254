﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_S10222254.Models
{
    public class Vote
    {
        public int BookId { get; set; }
        public string Justification { get; set; }
    }
}
