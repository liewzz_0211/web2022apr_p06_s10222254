﻿//Rental.cs is a simple text file under Models with properties of 2 classes RentalDiscount, Rental that will be used to contain data for the Rental Calculator
//RentalDiscount class is used to model the Discount checkbox choices on this page: 



using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace web_S10222254.Models
{
    //Validation attributes: Required, StringLength, Range, RegularExpression, Compare
    //Required: show user default message “The CustName field is required” if user do not enter anything into the Required field
    //StringLength: specifies maximum and minimum length of the property
    //Range: specifies minimum and maximum constraints for a numerical value. 

    //Regular: are an efficient way to enforce the shape and contents of a string value. 
    //e.g. [RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$")]  ->  property must use only letters, first letter upercase, white space, numbers and special characters are not allowed

    //Compare: two properties of a model object have the same value (e.g. property called "EmailConfirm", if Compare("Email"), string entered for property "EmailConfirm" must be same as "Email"





    //first Model class: RentalDiscount
    //Properties: Description, DiscountPercent, Selected
    public class RentalDiscount
    {
        public string Description { get; set; }
        public double DiscountPercent { get; set; }
        public bool Selected { get; set; }
    }





    //second Model class: Rental 
    //Properties: LoanDate, NumBooks, NumDays, RentalRate, DueDate, RentalFee, DiscountPercent, Discounts, AmountPayable
    public class Rental
    {
        //'Display' attribute sets the friendly display name for a model property
        //why Display attribute is used? e.g. if more than one View uses Rental model class, Display attribute set here will be called automatically in the more than 1 View through: asp-for="LoanDate"
        //asp-for will link LoanDate property to the label and call its set data to be printed
        [Display(Name = "Loan Date")]  //for this case, for the property "LoanDate", instead of displaying the name "LoanDate" in the label, we use 'Display' attribute to display a user-friendly name "Loan Date"


        //'DataType' attribute sets enables us to provide the runtime with information about the specific purpose of the property
        [DataType(DataType.Date)]  //for this case, we set 'DataType' attribute for property "LoanDate" as 'DataType.Date', thus only the date is displayed, not time information
                                   //if there was a property called "Password", where we do not want it to be in readable format, we can use 'DataType.Password' for its 'DataType' attribute to make it non-readable to users


        //'DisplayFormat' attribute handles various formatting options for the property
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]  //for this case, we will display property "LoanDate" in the format dd-MMM-yyyy
                                                               //we can also set 'ApplyFormatInEditMode' to true for 'DisplayFormat' attribute
                                                               //By default, ApplyFormatInEditMode is false because MVC binder might not to parse the value formatted for display only. 
        public DateTime LoanDate { get; set; }



        [Display(Name = "Number of Books")]
        public int NumBooks { get; set; }




        [Display(Name = "Number of Days")]
        public int NumDays { get; set; }


        
        [Display(Name = "Rental Rate (SGD)")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]  
        public double RentalRate { get; set; }
        


        [Display(Name = "Due Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime DueDate { get; set; }



        [Display(Name = "Rental Fee (SGD)")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        public double RentalFee { get; set; }  //The value for RentalFee will be displayed with 2 decimal places, and punctuated (,) at the thousand mark.
        




        [Display(Name = "Discount Given (%)")]
        [DisplayFormat(DataFormatString = "{0:#0.0}")]  //value for DiscountPercent will be displayed with 1 decimal places with no punctuation
                                                        //if value is 4, end value will be 4.0 with 0 added at the back
        public double DiscountPercent { get; set; }



        [Display(Name = "Discount Given (%)")]
        public List<RentalDiscount> Discounts { get; set; }



        [Display(Name = "Amount Payable (SGD)")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        public int AmountPayable { get; set; }
    }
}
