﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace web_S10222254.Models
{
    public class Staff
    {
        [Display(Name = "ID")]
        public int StaffID { get; set; }
        public int StaffId { get; internal set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public char Gender { get; set; }

        //this value can be nullable according to the "?"
        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        public DateTime? DOB { get; set; }

        public string Nationality { get; set; }

        [Display(Name = "Email Address")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}")]
        // Custom Validation Attribute for checking email address exists
        [ValidateEmailExists]
        public string Email { get; set; }

        [Range(1,10000)]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        [Display(Name = "Monthly Salary (SGD)")]
        public decimal Salary { get; set; }

        public bool IsFullTime { get; set; }

        public int? BranchNo { get; set; }
    }
}
